require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'csv', require: 'csv'
  gem 'git', require: 'git'
  gem "roo", require: 'roo'
  gem 'write_xlsx', require: 'write_xlsx'
end