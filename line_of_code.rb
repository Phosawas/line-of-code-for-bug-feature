load 'gemfile.rb'
load 'config.rb'

# ------------------ xlsx part --------------------
# define hash for map month to int
month = { "Jan" => 1, "Feb" => 2, "Mar" => 3, "Apr" => 4, "May" => 5, "Jun" => 6, "Jul" => 7, "Aug" => 8, "Sep" => 9, "Oct" => 10, "Nov" => 11, "Dec" => 12 }

# read story export xlsx
xlsx = Roo::Spreadsheet.open(@path_to_xlsx_file)

# array of data get from file .xlsx
ids = []
type = []
dates = []
# get colum from xlsx file fot 'id', 'type' and 'date'
xlsx.each_with_pagename do |name, sheet|
	ids = sheet.column(1)
	type = sheet.column(7)
	dates = sheet.column(10)
end
# remove column header
ids.delete_at(0)
type.delete_at(0)
dates.delete_at(0)

# flip array for match commit from git
ids = ids.reverse()
type = type.reverse()
dates = dates.reverse()

# get start date input and convert to Date
print 'Enter start date (dd-mm-yyyy) : '
start_date_input = gets.chomp
start_date = Date.strptime(start_date_input, '%d-%m-%Y')

#get end date input and convert to Date
print 'Enter end date (dd-mm-yyyy) : '
end_date_input = gets.chomp
end_date = Date.strptime(end_date_input, '%d-%m-%Y')

# array for select period
select_date_array = []
select_type_array = []
select_id_array = []
# selected ids, type and date to array from origin file array
dates.each_with_index do |date, index|
	date_from_file = date.split(' ')
	date_from_file[1] = date_from_file[1].delete ","
	file_date = Date.new(date_from_file[2].to_i, month[date_from_file[0]], date_from_file[1].to_i)
	if file_date.between?(start_date, end_date)
		select_id_array << "#{(ids[index.to_i]).to_i}"
		select_type_array << type[index.to_i]
		select_date_array << dates[index.to_i]
	end 
end

# ------------------ git part --------------------

# use to open file dir in local
git = Git.open(@path_to_git_local_dir)
# use to return commit that filter by start date input ( .log(value) default value is 30)
commit_array = git.log(2000000000).since(start_date)

# array for collect code edit
code_edit = []
# array of commit data
record = []
# array for collect all data that will write to file
all_records = []

# variable for count number of match for commit and story 
match_commit_count = 0
line_of_feature = 0
line_of_bug = 0

commit_array.each do |commit_id|
	# get current commit 
	commit = git.gcommit(commit_id)
	# find commit by match story id in commit message
	select_id_array.each_with_index do |id, index_id|

		if commit.message.include?("[##{id}]") || commit.message.include?("[#{id}]")
			data = []
			data[0] = id.to_i
			data[1] = select_type_array[index_id]
			data[2] = select_date_array[index_id]
			data[3] = commit.message
			data[4] = commit.date.strftime("%m-%d-%y")
			data[5] = commit.committer.name
			
			file_diff = git.diff(commit.parent, commit)

			commit_stat = file_diff.stats
			data[6] = commit_stat[:total][:insertions]
			data[7] = commit_stat[:total][:deletions]
			data[8] = commit_stat[:total][:lines]	
			
			code_edit << file_diff.patch
			record << data
			match_commit_count += 1
		end
	end
	unless record.empty?
		all_records << record
	end
end

code_edit.each_with_index do |code, index|
	byte_count = 0
	separate_code = code.split("\n")
	separate_code.each do |line|
		unless line.match(/^(\+|\-)[^+-]+/).nil?
			# check line byte count
			line[0] == "+" ? byte_count += line.bytesize : byte_count -= line.bytesize	
		end
	end

	record[index][9] = byte_count.abs
end

#---------------- file convert part ------------------

#new csv file
header = ['Story id', 'Story type', 'Story create date', 'Commit message', 'Commit date (M-D-Y)', 'Commiter', 'Line insertion', 'Line deletion', 'Line change', 'Byte change']

CSV.open("output.csv", "wb") do |csv|
	
	csv << header
	all_records.each_with_index do |record, index|
		csv << record[index] unless record[index].nil?
		
		unless record[index].nil?
			case record[index][1]
			when "feature"
				line_of_feature += record[index][8]
			when "bug"
				line_of_bug += record[index][8]
			end
		end
	end
	csv << []
	csv << ["Total match commit : #{match_commit_count}", "Line of feature : #{line_of_feature}", "Line of bug : #{line_of_bug}"]
end

#convert csv file to xlsx
workbook = WriteXLSX.new('output.xlsx')
worksheet = workbook.add_worksheet

row_index = 0
CSV.foreach("output.csv") do |row|
	worksheet.write_row(row_index, 0, row)
	row_index += 1
end

workbook.close